build: build-docker up

compose := docker-compose -f ./.docker/docker-compose.yml

build-docker:
	$(compose) build

up:
	$(compose) up -d
	$(MAKE) composer

ps:
	$(compose) ps


debug:
	$(compose) build --build-arg xdebug="true"
	$(MAKE) up

composer:
	$(compose) exec fpm composer install

lint:
	$(compose) exec fpm composer lint:php-cs
	$(compose) exec fpm composer lint:php-cs-fixer

test:
	$(compose) exec fpm composer test

migrate:
	$(compose) exec fpm bin/console doctrine:migrations:migrate --no-interaction

create-db:
	$(compose) exec fpm bin/console doctrine:database:create

nasa-sync:
	$(compose) exec fpm bin/console nasa:sync