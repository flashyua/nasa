## What's this? ##
This is test project for work with NASA API

## How to run? ##
1. clone repository git@gitlab.com:flashyua/nasa.git
2. run `make` for launch docker-compose environment
3. run `make create-db` for create database for project
4. run `make migrate` for run migrations for database

## Usage ##
### Sync with NASA ###
Run `bin/console nasa:sync` in container to sync with fresh nasa data. This command takes an argument how many days take to sync.
This command also has alias `make nasa-sync`

### Endpoints ###
1. `GET /neo/hazardous` - returns all DB entries which contain potentially hazardous asteroids (with pagination). Use `limit` and `offset` parameters. Default values is 10 and 0.
2. `GET /neo/fastest?hazardous=(true|false)` - returns fastest asteroid from hazardous or no hazardous asteroids. Default value is  `false`.
3. `GET /neo/best-month?hazardous=(true|false)` - returns a month with most asteroids (not a month in a year). Default value for `hazardous` is  `false`.

## Tests ##
Use `make test` for launched project to run functional tests
Use `make lint` to run codestyle checking

## Debug ##
1. Use `mae debug` to create fpm container with xdebug installation
2. Run `sudo ifconfig en0 alias 10.254.254.254 255.255.255.0` on host machine to create network connection between container and host
3. Configure IDE Settings: `IDE Key = PHPSTORM; Debug port = 9001; Server name = nasa-fpm`