<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200921113221 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
        CREATE TABLE asteroids (
            id INT AUTO_INCREMENT NOT NULL, 
            reference VARCHAR(16) NOT NULL, 
            date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
            speed DOUBLE PRECISION NOT NULL, 
            is_hazardous TINYINT(1) NOT NULL, 
            PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE asteroids');
    }
}
