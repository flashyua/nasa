<?php

declare(strict_types=1);

namespace App\Client;

use App\Entity\Asteroid;
use App\Hydrator\AsteroidHydrator;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

class NasaClient
{
    private ClientInterface $client;
    private AsteroidHydrator $hydrator;
    private string $path;
    private string $token;

    /**
     * NasaClient constructor.
     * @param ClientInterface $client
     * @param AsteroidHydrator $hydrator
     * @param string $path
     * @param string $token
     */
    public function __construct(ClientInterface $client, AsteroidHydrator $hydrator, string $path, string $token)
    {
        $this->client = $client;
        $this->hydrator = $hydrator;
        $this->path = $path;
        $this->token = $token;
    }

    /**
     * @return Asteroid[]
     *
     * @throws ClientException
     */
    public function fetchAsteroids(\DateTime $from, \DateTime $to): array
    {
        try {
            $response = $this->client->request('GET', $this->path.'/feed', [
                'query' => [
                    'api_key' => $this->token,
                    'start_date' => $from->format('Y-m-d'),
                    'end_date' => $to->format('Y-m-d'),
                ],
            ]);
        } catch (GuzzleException $e) {
            throw new ClientException('Cannot fetch asteroids from API', $e->getCode(), $e);
        }

        $data = json_decode($response->getBody()->getContents(), true);

        $asteroids = [];
        foreach ($data['near_earth_objects'] as $asteroidsByDate) {
            $asteroids = array_merge($asteroids, $this->hydrator->hydrateMany($asteroidsByDate));
        }

        return $asteroids;
    }
}
