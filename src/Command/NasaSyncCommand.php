<?php

declare(strict_types=1);

namespace App\Command;

use App\Client\NasaClient;
use App\Repository\AsteroidsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NasaSyncCommand extends Command
{
    const DEFAULT_DAYS_TO_COLLECT = 3;

    private NasaClient $client;
    private EntityManagerInterface $em;
    private AsteroidsRepository $repository;

    /**
     * NasaSyncCommand constructor.
     */
    public function __construct(NasaClient $client, EntityManagerInterface $em, AsteroidsRepository $repository)
    {
        $this->client = $client;
        $this->em = $em;
        $this->repository = $repository;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('nasa:sync');
        $this->addArgument(
            'days',
            InputArgument::OPTIONAL,
            'Days for collect asteroids',
            self::DEFAULT_DAYS_TO_COLLECT
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $daysToCollect = (int) $input->getArgument('days');
        $asteroids = $this->client->fetchAsteroids(new \DateTime("-{$daysToCollect} day"), new \DateTime());
        foreach ($asteroids as $asteroid) {
            if (!$this->repository->exists($asteroid)) {
                $this->em->persist($asteroid);
                $output->writeln('Added new asteroid, referehce '.$asteroid->getReference());
            }
        }
        $this->em->flush();
        $output->writeln('Asteroids list succesfully synced!');
    }
}
