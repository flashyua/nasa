<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\AsteroidsRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NeoController extends AbstractController
{
    private AsteroidsRepository $asteroidsRepository;
    private SerializerInterface $serializer;

    public function __construct(AsteroidsRepository $asteroidsRepository, SerializerInterface $serializer)
    {
        $this->asteroidsRepository = $asteroidsRepository;
        $this->serializer = $serializer;
    }

    /**
     * @Route(path="neo/hazardous")
     * @param Request $request
     * @return Response
     */
    public function getHazardous(Request $request): Response
    {
        $offset = (int) $request->query->get('offset', 0);
        $limit = (int) $request->query->get('limit', 10);
        $asteroids = $this->asteroidsRepository->getHazardousQuery()
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getResult();

        $json = $this->serializer->serialize($asteroids, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route(path="neo/fastest")
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getFastest(Request $request): Response
    {
        $isHazardous = 'false' === $request->query->get('hazardous', false) ? false : true;
        $asteroid = $this->asteroidsRepository->getFastestQuery($isHazardous)
            ->setMaxResults(1)
            ->getOneOrNullResult();

        $json = $this->serializer->serialize(['fastest' => $asteroid], 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route(path="neo/best-month")
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getBestMonth(Request $request): Response
    {
        $isHazardous = 'false' === $request->query->get('hazardous', false) ? false : true;

        $month = $this->asteroidsRepository->getBestMonthQuery($isHazardous)
            ->setMaxResults(1)
            ->getOneOrNullResult();

        $json = $this->serializer->serialize(['best-month' => $month], 'json');

        return new JsonResponse($json, 200, [], true);
    }
}
