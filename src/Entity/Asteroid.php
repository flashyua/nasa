<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="asteroids")
 */
class Asteroid
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="reference", type="string", length=16)
     */
    private string $reference;

    /**
     * @var \DateTimeImmutable | null
     * @ORM\Column(name="date", type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $date;

    /**
     * @ORM\Column(name="speed", type="float")
     */
    private float $speed;

    /**
     * @ORM\Column(name="is_hazardous", type="boolean")
     */
    private bool $isHazardous;

    /**
     * Asteroid constructor.
     *
     * @param string $reference
     * @param \DateTimeImmutable $date
     * @param float $speed
     * @param bool $isHazardous
     */
    public function __construct(string $reference, ?\DateTimeImmutable $date, float $speed, bool $isHazardous)
    {
        $this->reference = $reference;
        $this->date = $date;
        $this->speed = $speed;
        $this->isHazardous = $isHazardous;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return float
     */
    public function getSpeed(): float
    {
        return $this->speed;
    }

    /**
     * @return bool
     */
    public function isHazardous(): bool
    {
        return $this->isHazardous;
    }
}
