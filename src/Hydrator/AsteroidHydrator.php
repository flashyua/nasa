<?php

declare(strict_types=1);

namespace App\Hydrator;

use App\Entity\Asteroid;

class AsteroidHydrator
{
    public function hydrateOne(array $asteroid): Asteroid
    {
        $date = null;
        $velocity = 0.0;
        foreach ($asteroid['close_approach_data'] as $approach) {
            $approachDate = is_null($approach['close_approach_date_full']) ? null : new \DateTimeImmutable();
            if (is_null($date) || $approachDate > $date) {
                $date = $approachDate;
                $velocity = $approach['relative_velocity']['kilometers_per_hour'];
            }
        }

        return new Asteroid(
            $asteroid['neo_reference_id'],
            $date,
            (float) $velocity,
            $asteroid['is_potentially_hazardous_asteroid']
        );
    }

    /**
     * @return Asteroid[]
     */
    public function hydrateMany(array $asteroids): array
    {
        $asteroids =  array_map(function ($asteroid) {
            return $this->hydrateOne($asteroid);
        }, $asteroids);

        return $asteroids;
    }
}
