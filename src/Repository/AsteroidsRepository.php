<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Asteroid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

class AsteroidsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Asteroid::class);
    }

    public function getHazardousQuery(bool $isHazardous = true): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->andWhere($qb->expr()->eq('a.isHazardous', ':isHazardous'))
            ->setParameter('isHazardous', $isHazardous)
            ->getQuery();
    }

    public function getFastestQuery(bool $isHazardous): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->andWhere($qb->expr()->eq('a.isHazardous', ':isHazardous'))
            ->orderBy('a.speed', 'DESC')
            ->setParameter('isHazardous', $isHazardous)
            ->getQuery();
    }

    public function getBestMonthQuery(bool $isHazardous): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('DATE_FORMAT(a.date, \'%Y-%m\') as year_month, count(a) as total')
            ->andWhere($qb->expr()->eq('a.isHazardous', ':isHazardous'))
            ->groupBy('year_month')
            ->orderBy('year_month', 'DESC')
            ->setParameter('isHazardous', $isHazardous)
            ->getQuery();
    }

    public function exists(Asteroid $asteroid): bool
    {
        $existed = $this->findOneBy([
            'reference' => $asteroid->getReference(),
        ]);

        return !empty($existed);
    }
}
