<?php

declare(strict_types=1);

namespace App\Tests\functional;

use App\Entity\Asteroid;
use App\Repository\AsteroidsRepository;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NeoControllerTest extends WebTestCase
{
    private SerializerInterface $serializer;
    private AsteroidsRepository $asteroidsRepository;
    private EntityManagerInterface $entityManager;

    public function setUp(): void
    {
        self::bootKernel();
        $this->serializer = static::$container->get(SerializerInterface::class);
        $this->asteroidsRepository = static::$container->get(AsteroidsRepository::class);
        $this->entityManager = static::$container->get(EntityManagerInterface::class);
    }

    /**
     * @dataProvider hazardousDataProvider
     */
    public function testHazardous(int $offset, int $limit)
    {
        $client = static::createClient();

        $client->request('GET', '/neo/hazardous', [
            'offset' => $offset,
            'limit' => $limit,
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $asteroids = $this->entityManager
            ->createQueryBuilder()
            ->select('a')
            ->from(Asteroid::class, 'a')
            ->andWhere('a.isHazardous = :isHazardous')
            ->setParameter('isHazardous', true)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
        $expectedResponse = $this->serializer->serialize($asteroids, 'json');

        $this->assertEquals($expectedResponse, $client->getResponse()->getContent());
    }

    /**
     * @dataProvider fastestDataProvider
     *
     * @param $isHazardous
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testFastest($isHazardous)
    {
        $client = static::createClient();

        if (is_null($isHazardous)) {
            $client->request('GET', '/neo/fastest');
        } else {
            $client->request('GET', '/neo/fastest', [
                'hazardous' => $isHazardous ? 'true' : 'false',
            ]);
        }

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $asteroid = $this->entityManager
            ->createQueryBuilder()
            ->select('a')
            ->from(Asteroid::class, 'a')
            ->andWhere('a.isHazardous = :isHazardous')
            ->setParameter('isHazardous', is_null($isHazardous) ? false : $isHazardous)
            ->orderBy('a.speed', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
        $expectedResponse = $this->serializer->serialize($asteroid, 'json');

        $this->assertEquals($expectedResponse, $client->getResponse()->getContent());
    }

    /**
     * @dataProvider bestMonthDataProvider
     *
     * @param $isHazardous
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testBestMonth($isHazardous)
    {
        $client = static::createClient();

        if (is_null($isHazardous)) {
            $client->request('GET', '/neo/best-month');
        } else {
            $client->request('GET', '/neo/best-month', [
                'hazardous' => $isHazardous ? 'true' : 'false',
            ]);
        }

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $months = $this->entityManager
            ->createQueryBuilder()
            ->select('DATE_FORMAT(a.date, \'%Y-%m\') as year_month, count(a) as total')
            ->from(Asteroid::class, 'a')
            ->andWhere('a.isHazardous = :isHazardous')
            ->groupBy('year_month')
            ->orderBy('year_month', 'DESC')
            ->setParameter('isHazardous', is_null($isHazardous) ? false : $isHazardous)
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();

        $expectedResponse = $this->serializer->serialize($months, 'json');

        $this->assertEquals($expectedResponse, $client->getResponse()->getContent());
    }

    public function hazardousDataProvider()
    {
        return [
            [0, 10],
            [10, 5],
        ];
    }

    public function fastestDataProvider()
    {
        return [
            [null],
            [true],
            [false],
        ];
    }

    public function bestMonthDataProvider()
    {
        return [
            [null],
            [true],
            [false],
        ];
    }
}
